<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => 'Spn4\Service\Controllers', 'middleware' => 'web'], function () {
    Route::group(['prefix' => 'install'], function(){
        Route::get('pre-requisite', 'InstallController@preRequisite')->name('service.preRequisite');
        Route::get('database', 'InstallController@database')->name('service.database');
        Route::post('database', 'InstallController@post_database');
        Route::get('done', 'InstallController@done')->name('service.done');
    });

    Route::post('/install-adons', 'InstallController@ManageAddOnsValidation')->name('ManageAddOnsValidation');

});


