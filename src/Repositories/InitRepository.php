<?php

namespace Spn4\Service\Repositories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class InitRepository {

    public function init() {
        
    }

    public function checkDatabase(){

        try {
            if (!Storage::has('settings.json')) {
                DB::connection()->getPdo();
                if (!Schema::hasTable(config('spn4.settings_table')) || !Schema::hasTable('users')){
                    return false;
                }
            }
        } catch(\Exception $e){
            $error = $e->getCode();
            if($error == 2002){
                 abort(403, 'No connection could be made because the target machine actively refused it');
            } else if($error == 1045){
                $c = Storage::exists('.app_installed') && Storage::get('.app_installed');
                if($c){
                     abort(403, 'Access denied for user. Please check your database username and password.');
                }

            }
        }

        return true;
    }


}
