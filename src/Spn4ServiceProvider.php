<?php

namespace Spn4\Service;

use Illuminate\Support\ServiceProvider;
use Spn4\Service\Console\Commands\MigrateStatusCommand;
use Spn4\Service\Middleware\ServiceMiddleware;

class Spn4ServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', ServiceMiddleware::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'service');
        $this->loadViewsFrom(resource_path('/views/vendors/service'), 'service');

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/service'),
             __DIR__.'/../resources/views' => resource_path('views/vendors/service'),
        ], 'spn4');

        $this->commands([
            MigrateStatusCommand::class,
        ]);
    }
}

