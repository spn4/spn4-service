<?php

namespace Spn4\Service\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spn4\Service\Repositories\InitRepository as ServiceRepository;
use Illuminate\Support\Facades\Storage;

class ServiceMiddleware
{
    protected $repo, $service_repo;

    public function __construct(
        ServiceRepository $service_repo
    ) {
        $this->service_repo = $service_repo;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logout = Storage::exists('.logout') && Storage::get('.logout');
        if ($logout) {
            $request->session()->flush();
            Storage::delete('.logout');
            return redirect('/install');
        }

    

        return $next($request);
    }
}
