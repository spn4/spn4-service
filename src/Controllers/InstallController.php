<?php

namespace Spn4\Service\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Spn4\Service\Repositories\InstallRepository;
use Spn4\Service\Requests\DatabaseRequest;
use Spn4\Service\Requests\LicenseRequest;
use Spn4\Service\Requests\UserRequest;
use Spn4\Service\Requests\ModuleInstallRequest;

class InstallController extends Controller{
    protected $repo, $request;

    public function __construct(InstallRepository $repo, Request $request)
    {
        $this->repo = $repo;
        $this->request = $request;
    }


    public function preRequisite(){

        $ac = Storage::exists('.app_installed') ? Storage::get('.app_installed') : null;
        if($ac){
            abort(404);
        }
        $checks = $this->repo->getPreRequisite();
		$server_checks = $checks['server'];
		$folder_checks = $checks['folder'];
        $has_false = in_array(false, $checks);

		envu(['APP_ENV' => 'production']);
		$name = env('APP_NAME');
		return view('service::install.preRequisite', compact('server_checks', 'folder_checks', 'name', 'has_false'));
    }

    public function database(){

        $checks = $this->repo->getPreRequisite();

        if(in_array(false, $checks)){
            return redirect()->route('service.preRequisite')->with(['message' => __('service::install.requirement_failed'), 'status' => 'error']);
        }

        $ac = Storage::exists('.app_installed') ? Storage::get('.app_installed') : null;

        if($ac){
            abort(404);
        }

        if ($this->repo->checkDatabaseConnection()) {
            return redirect()->route('service.user')->with(['message' => __('service::install.connection_established'), 'status' => 'success']);
        }

		return view('service::install.database');
    }

    public function post_database(DatabaseRequest $request){
        $this->repo->validateDatabase($request->all());
		return response()->json(['message' => __('service::install.connection_established'), 'goto' => route('service.user')]);
    }


    public function done(){

        $data['user'] = Storage::exists('.user_email') ? Storage::get('.user_email') : null;
        $data['pass'] = Storage::exists('.user_pass') ? Storage::get('.user_pass') : null;

        if($data['user'] and $data['pass']){
            Log::info('done');
            Storage::delete(['.user_email', '.user_pass']);
            Storage::put('.install_count', 1);
            return view('service::install.done', $data);
        } else{
            abort(404);
        }

    }

    public function ManageAddOnsValidation(ModuleInstallRequest $request){
        $response = $this->repo->installModule($request->all());
        return response()->json(['message' => __('service::install.module_verify'), 'reload' => true]);
    }

}
